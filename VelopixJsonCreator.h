#pragma once

#include <fstream>
#include <vector>
#include <string>
#include <cstdio>
#include "VelopixTypecast.h"
#include "FileReader.h"

class VelojsonCreator {
private:
	int fileNumber = 0;
	std::string foldername = "./";
	
	std::string tab (const int& int_tab, const std::string& str_tab = "  ") {
		if (int_tab < 0) throw StrException("Error: int_tab < 0");
		std::string temp = "";
		for (int i=0; i<int_tab; ++i) temp += str_tab;
		return temp;
	};

	void writeline (
		std::ofstream& f,
		const int& int_tab,
		const std::string& value_name,
		const uint32_t value,
		const bool& last = false
	) {
		const std::string t = tab(int_tab);
		f << t << "\"" << value_name << "\": " << std::to_string(value);
		if (!last) f << ",";
		f << std::endl;
	}

	template<class T>
	void writeline (
		std::ofstream& f,
		const int& int_tab,
		const std::string& vector_name,
		const T* v,
		const int size,
		const bool& last = false
	) {
		const std::string t = tab(int_tab);
		f << t << "\"" << vector_name << "\": [";
		for (int i=0; i<size - 1; ++i) {
			f << std::to_string(v[i]) << ", ";
		}
		f << std::to_string(v[size-1]) << "]";
		if (!last) f << ",";
		f << std::endl;
	}

public:
	VelojsonCreator () {}
	VelojsonCreator (const std::string& foldername) :
		foldername(foldername) {}

	void generate (const VelopixMCInstance& instance) {
		const std::string filename = foldername + "/" + std::to_string(fileNumber) + ".json";
		std::ofstream f;
		f.open(filename.c_str());

		int int_tab = 0;
		f << tab(int_tab++) << "{" << std::endl;
		
		// Event section
		f << tab(int_tab++) << "\"event\": {" << std::endl;
		writeline(f, int_tab, "number_of_sensors", instance.number_of_sensors);
		writeline(f, int_tab, "number_of_hits", instance.number_of_hits);
		writeline(f, int_tab, "sensor_module_z", instance.sensor_Zs, instance.number_of_sensors);
		writeline(f, int_tab, "sensor_hits_starting_index", instance.sensor_hitStarts, instance.number_of_sensors);
		writeline(f, int_tab, "sensor_number_of_hits", instance.sensor_hitNums, instance.number_of_sensors);
		writeline(f, int_tab, "hit_id", instance.hit_IDs, instance.number_of_hits);
		writeline(f, int_tab, "hit_x", instance.hit_Xs, instance.number_of_hits);
		writeline(f, int_tab, "hit_y", instance.hit_Ys, instance.number_of_hits);
		writeline(f, int_tab, "hit_z", instance.hit_Zs, instance.number_of_hits, true);
		f << tab(--int_tab) << "}," << std::endl;

		// MC particle section
		f << tab(int_tab++) << "\"montecarlo\": {" << std::endl;
		f << tab(int_tab) << "\"description\": [\"mcp_key\", \"mcp_id\", \"mcp_p\", \"mcp_pt\", \"mcp_eta\", \"mcp_phi\", "
			<< "\"mcp_islong\", \"mcp_isdown\", \"mcp_isvelo\", \"mcp_isut\", \"mcp_strangelong\", \"mcp_strangedown\", \"mcp_fromb\", \"mcp_fromd\","
			<< "\"mcp_no_hits\", \"mcp_hits\"]," << std::endl;
		f << tab(int_tab++) << "\"particles\": [" << std::endl;

		for (int i=0; i<instance.no_mcp; ++i) {
			const MCParticle& mcp = instance.mc_particles[i];
			const bool islast = i==instance.no_mcp-1;

			f << tab(int_tab) << "["
				<< std::to_string(mcp.mcp_key) << ", "
				<< std::to_string(mcp.mcp_id) << ", "
				<< std::to_string(mcp.mcp_p) << ", "
				<< std::to_string(mcp.mcp_pt) << ", "
				<< std::to_string(mcp.mcp_eta) << ", "
				<< std::to_string(mcp.mcp_phi) << ", "
				<< std::to_string(mcp.mcp_islong) << ", "
				<< std::to_string(mcp.mcp_isdown) << ", "
				<< std::to_string(mcp.mcp_isvelo) << ", "
				<< std::to_string(mcp.mcp_isut) << ", "
				<< std::to_string(mcp.mcp_strangelong) << ", "
				<< std::to_string(mcp.mcp_strangedown) << ", "
				<< std::to_string(mcp.mcp_fromb) << ", "
				<< std::to_string(mcp.mcp_fromd) << ", "
				<< std::to_string(mcp.no_hits) << ", "
				<< "[";
			for (int i=0; i<mcp.no_hits; ++i) {
				const bool nohits_islast = i==mcp.no_hits-1;
				f << std::to_string(mcp.hitIDs[i]) << (nohits_islast ? "" : ", ");
			}
			f << "]]" << (islast ? "" : ",") << std::endl;
		}
		
		f << tab(--int_tab) << "]" << std::endl;	
		f << tab(--int_tab) << "}" << std::endl;	
		f << tab(--int_tab) << "}" << std::endl;		

		f.close();
		++fileNumber;
	}
};
