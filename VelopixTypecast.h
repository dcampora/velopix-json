#pragma once

#include <cstdint>
#include <vector>
#include <iostream>

struct MCParticle {
  uint32_t mcp_key;
  uint32_t mcp_id;
  float mcp_p;
  float mcp_pt;
  float mcp_eta;
  float mcp_phi;
  uint8_t mcp_islong;
  uint8_t mcp_isdown;
  uint8_t mcp_isvelo;
  uint8_t mcp_isut;
  uint8_t mcp_strangelong;
  uint8_t mcp_strangedown;
  uint8_t mcp_fromb;
  uint8_t mcp_fromd;
  uint32_t no_hits;
  uint32_t* hitIDs;

  MCParticle () {}
  MCParticle (uint8_t*& i) {
    mcp_key = *((uint32_t*) i); i += sizeof(uint32_t);
    mcp_id  = *((uint32_t*) i); i += sizeof(uint32_t);
    mcp_p   = *((float*) i); i += sizeof(float);
    mcp_pt  = *((float*) i); i += sizeof(float);
    mcp_eta = *((float*) i); i += sizeof(float);
    mcp_phi = *((float*) i); i += sizeof(float);
    mcp_islong = *((uint8_t*) i); i += sizeof(uint8_t);
    mcp_isdown = *((uint8_t*) i); i += sizeof(uint8_t);
    mcp_isvelo = *((uint8_t*) i); i += sizeof(uint8_t);
    mcp_isut   = *((uint8_t*) i); i += sizeof(uint8_t);
    mcp_strangelong = *((uint8_t*) i); i += sizeof(uint8_t);
    mcp_strangedown = *((uint8_t*) i); i += sizeof(uint8_t);
    mcp_fromb  = *((uint8_t*) i); i += sizeof(uint8_t);
    mcp_fromd  = *((uint8_t*) i); i += sizeof(uint8_t);
    no_hits = *((uint32_t*) i); i += sizeof(uint32_t);
    hitIDs  = (uint32_t*) i; i += no_hits * sizeof(uint32_t);
  }
};

struct MCTracks {
  uint32_t no_hits;
  uint32_t* hitIDs;

  MCTracks () {}
  MCTracks (uint8_t*& i) {
    no_hits = *((uint32_t*) i); i += sizeof(uint32_t);
    hitIDs  = (uint32_t*) i; i += no_hits * sizeof(uint32_t);
  }
};

struct VelopixMCInstance {
  uint8_t* _input;
  size_t _size;

  uint32_t funcNameLen;
  char* funcName;
  uint32_t dataSize;

  uint32_t number_of_sensors;
  uint32_t number_of_hits;
  int32_t* sensor_Zs;
  uint32_t* sensor_hitStarts;
  uint32_t* sensor_hitNums;
  float* hit_Xs;
  float* hit_Ys;
  float* hit_Zs;
  uint32_t* hit_IDs;

  uint32_t no_mcp;
  std::vector<MCParticle> mc_particles;

  uint32_t no_mctracks;
  std::vector<MCTracks> mc_tracks;

  VelopixMCInstance () {}
  VelopixMCInstance (uint8_t* input, size_t size) :
    _input(input), _size(size) {

    uint8_t* end = _input + _size;
    uint8_t* i = _input;
    
    funcNameLen = *((uint32_t*) i); i += sizeof(uint32_t);
    funcName    = (char*) i;        i += funcNameLen * sizeof(char);
    dataSize    = *((uint32_t*) i); i += sizeof(uint32_t);

    number_of_sensors = *((uint32_t*) i); i += sizeof(uint32_t);
    number_of_hits    = *((uint32_t*) i); i += sizeof(uint32_t);
    sensor_Zs         = (int32_t*) i; i += sizeof(int32_t) * number_of_sensors;
    sensor_hitStarts  = (uint32_t*) i; i += sizeof(uint32_t) * number_of_sensors;
    sensor_hitNums    = (uint32_t*) i; i += sizeof(uint32_t) * number_of_sensors;
    hit_IDs           = (uint32_t*) i; i += sizeof(uint32_t) * number_of_hits;
    hit_Xs            = (float*)  i; i += sizeof(float)    * number_of_hits;
    hit_Ys            = (float*)  i; i += sizeof(float)    * number_of_hits;
    hit_Zs            = (float*)  i; i += sizeof(float)    * number_of_hits;

    if (i < end) {
      no_mcp = *((uint32_t*) i); i += sizeof(uint32_t);
      mc_particles.resize(no_mcp);

      for (int j=0; j<no_mcp; ++j) {
        mc_particles[j] = MCParticle(i);
      }
    }

    if (i < end) {
      no_mctracks = *((uint32_t*) i); i += sizeof(uint32_t);
      mc_tracks.resize(no_mctracks);

      for (int j=0; j<no_mctracks; ++j) {
        mc_tracks[j] = MCTracks(i);
      }
    }

    if (i != end) {
      std::cerr << "i and end differ: " << static_cast<void*>(i) << ", " << static_cast<void*>(end) << std::endl;
    }
  }
};
