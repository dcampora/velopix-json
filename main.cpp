#include <string>
#include "VelopixJsonCreator.h"
#include "VelopixTypecast.h"
#include "FileReader.h"

int main (int argc, char** argv) {
	std::string folderName = "defaultFolderName";
	std::string outputFolderName = "./";
	int requestedFiles = 1;

	if (argc >= 2) {
		folderName = argv[1];
	}
	if (argc >= 3) {
		requestedFiles = atoi(argv[2]);
	}
	if (argc >= 4) {
		outputFolderName = argv[3];
	}

	// Read files
	FileReader fr;
	std::vector<std::vector<uint8_t>> filecontents (requestedFiles);
	fr.readFilesFromFolder(filecontents, folderName, requestedFiles, "dat");

	// Typecast files
	std::vector<VelopixMCInstance> velopixInstances (requestedFiles);
	for (int i=0; i<requestedFiles; ++i) {
		velopixInstances[i] = VelopixMCInstance (filecontents[i].data(), filecontents[i].size());
	}

	// Generate json files
	std::cout << "Writing out json files to " << outputFolderName << "..." << std::endl;
	VelojsonCreator json {outputFolderName};
	for (const auto& instance : velopixInstances) {
		json.generate(instance);
	}

	std::cout << "All is success. Good." << std::endl;

	return 0;
}
